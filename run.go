package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type User struct {
	Id int
	Name string
}

func main(){
	r := mux.NewRouter()
	r.HandleFunc("/", UserServer2)
	http.Handle("/", r)
	fmt.Println("Starting up on 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
	//http.HandleFunc("/", UserServer)
	//http.ListenAndServe(":8080", nil)
}

func UserServer(w http.ResponseWriter, r *http.Request){
	var user User
	user.Id = 1
	user.Name = "Dar"
	userJson, err := json.Marshal(user)
	if err != nil {
		panic(err.Error())
	}
	//fmt.Println(w, string(userJson))
	fmt.Fprintf(w, string(userJson))

}

func UserServer2(w http.ResponseWriter, req *http.Request) {
	var user User
	user.Id = 2
	user.Name = "Dariya"
	userJson, err := json.Marshal(user)
	if err != nil {
		panic(err.Error())
	}
	//fmt.Println(w, string(userJson))
	fmt.Fprintf(w, string(userJson))
}
